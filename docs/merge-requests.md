## Working with Code

The process of submitting a merge request is fairly straightforward and generally follows the same pattern each time:

1. [Create a new branch](#step1)
2. [Make your changes](#step2)
3. [Rebase onto upstream](#step3)
4. [Run the tests](#step4)
5. [Double check your submission](#step5)
6. [Push your changes](#step6)
7. [Submit the merge request](#step7)

Details about each step are found below.

### Step 1: Create a new branch<a name="step1"></a>

TODO

### Step 2: Make your changes<a name="step2"></a>

TODO

### Step 3: Rebase onto upstream<a name="step3"></a>

TODO

### Step 4: Run the tests<a name="step4"></a>

TODO

### Step 5: Double check your submission<a name="step5"></a>

TODO

### Step 6: Push your changes<a name="step6"></a>

TODO

### Step 7: Send the merge request<a name="step7"></a>

Now you're ready to send the merge request.

## Following Up

Once your merge request is sent, it's time for the team to review it. As such, please make sure to:

1. Monitor the status of the CI build for your merge request. If it fails, please investigate why. We cannot merge pull requests that fail CI for any reason.
1. Respond to comments left on the pull request from team members. Remember, we want to help you land your code, so please be receptive to our feedback.
1. We may ask you to make changes, rebase, or squash your commits.

### Updating the Code

If we ask you to make code changes, there's no need to close the pull request and create a new one. Just go back to the branch and make your changes. Then, when you're ready, you can add your changes into the branch:

```
$ git add -A
$ git commit
$ git push origin feature/1234
```
### Rebasing

If your code is out-of-date, we might ask you to rebase. That means we want you to apply your changes on top of the latest upstream code. Make sure you have set up a [development environment](./development-environment.md) and then you can rebase using these commands:

```
$ git fetch upstream
$ git rebase upstream/master
```

You might find that there are merge conflicts when you attempt to rebase. Please [resolve the conflicts](https://help.github.com/articles/resolving-merge-conflicts-after-a-git-rebase/) and then do a forced push to your branch:

```
$ git push origin feature/1234 -f
```



Based on https://github.com/eslint/eslint/blob/master/docs/developer-guide/contributing/pull-requests.md