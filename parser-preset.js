module.exports = {
  parserOpts: {
    headerPattern: /^(\w*)(?:\((\w*)\))?:\s(.*)\s?(?:\((?:refs |fixes )?(?:\s?,?\s?#(\w+))+\))?$/,
    headerCorrespondence: ['type', 'scope', 'subject', 'ticket' ]
  }
};