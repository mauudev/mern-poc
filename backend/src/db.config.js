import mysql from 'mysql'

const dbConn = mysql.createConnection({
    host     : 'localhost',
    user     : 'user',
    password : 'pass',
    database : 'test_db',
    port: 3307
  });

export default async () => {
    await dbConn.connect()
      .then(console.info('Successful connected to database.'))
      .catch((err) => {
        console.error(err.message || 'Some error occurred while connect to database');
      });
  };
  

//local mysql db connection
// const dbConn = mysql.createConnection({
//   host     : 'localhost',
//   user     : 'user',
//   password : 'pass',
//   database : 'test_db',
//   port: 3307
// });

// dbConn.connect(function(err) {
//   if (err) throw err;
//   console.log("Database Connected!");
// });

// module.exports = dbConn;
