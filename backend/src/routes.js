import express from 'express';
import systemRoutes from './components/system/system.api';

const router = express.Router();
router.use('/v1', systemRoutes);

export default router;
