import express from 'express';
import httpStatus from 'http-status';
import SystemDal from './system.dal';

const router = express.Router();
const system = new SystemDal();

router.get('/system', (req, res) => {
  res.status(httpStatus.OK).send(system.information);
});

export default router;
