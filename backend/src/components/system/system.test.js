import assert from 'assert';
import os from 'os';
import SystemDal from './system.dal';

describe('System Component', () => {
  let system;
  beforeEach(() => {
    system = new SystemDal();
  });
  describe('System Information.', () => {
    it('Get system plataform.', () => {
      const expectedResult = os.platform();
      assert.equal(expectedResult, system.information.platform);
    });
  });
});
