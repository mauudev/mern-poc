import os from 'os';

class System {
  constructor() {
    this._osInfo = {
      platform: os.platform(),
      type: os.type(),
      upTime: os.uptime(),
      hostName: os.hostname(),
    };
  }
  get information() {
    return this._osInfo;
  }
}

export default System;
