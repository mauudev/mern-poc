import express from 'express';
import morgan from 'morgan';
import compression from 'compression';
import methodOverride from 'method-override';
import helmet from 'helmet';
import cors from 'cors';

import config from './config';
import routes from './routes';
import userAuthentication from './middlewares/authentication';

const app = express();

app.use(morgan(config.get('LOG_FORMAT')));
app.use(express.urlencoded({extended: true}));
app.use(express.json()) // To parse the incoming requests with JSON payloads
app.use(compression());
app.use(methodOverride());
app.use(helmet());
app.use(cors());

// Setting application meddlewares
app.use(userAuthentication);
// Adding application routes
app.use('/api', routes);

export default app;
