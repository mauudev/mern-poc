import nconf from 'nconf';
import Joi from 'joi';

const envVarsSchema = Joi.object({
  NODE_ENV: Joi.string()
    .allow(['development', 'production'])
    .default('development'),
  PORT: Joi.number()
    .default(3000),
  LOG_FORMAT: Joi.string()
    .when('NODE_ENV', {
      is: Joi.string().equal('production'),
      then: Joi.string().default('combined'),
      otherwise: Joi.string().default('dev'),
    }),
  JWT_SECRET: Joi.string()
    // .required()
    .description('JWT Secret'),
  DB_HOST: Joi.string()
    // .required()
    .default('localhost')
    .description('Database host'),
  DB_USER: Joi.string()
    // .required()
    .default('user')
    .description('Database user'),
  DB_PASSWORD: Joi.string()
    // .required()
    .default('pass')
    .description('Database password'),
  DB_DATABASE: Joi.string()
    // .required()
    .default('test_db')
    .description('Default database'),
  DB_PORT: Joi.number()
    .default(3307),
}).unknown().required();

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema);

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

nconf
  // 1. Command-line arguments
  .argv()
  // 2. Environment variables
  .env([
    'NODE_ENV',
    'PORT',
    'JWT_SECRET',
    'DB_HOST',
    'DB_USER',
    'DB_PASSWORD',
    'DB_DATABASE',
    'DB_PORT'
  ])
  // 4. Defaults variables
  .defaults(envVars);

export default nconf;
