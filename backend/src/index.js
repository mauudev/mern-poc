import app from './express';
import config from './config';

app.listen(config.get('PORT'), () => console.info(`Server running on port ${config.get('PORT')}.`));

export default app;
