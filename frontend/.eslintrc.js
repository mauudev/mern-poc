module.exports = {
  "rules": {
    semi: ["error", "always"],
    "space-infix-ops": ["error", {"int32Hint": false}],
    "no-console": 0,
    "no-underscore-dangle": 0,
    "no-unused-vars": ["error", { "argsIgnorePattern": "next" }],
    //"no-use-before-define": ["error", { "variables": false }],
    "no-multi-str": 0,
    "curly": [2, "all"],
    "brace-style": [2, "1tbs"],
    "class-methods-use-this": 0
  },
  "env": {
    "browser": true,
    "node": true,
    "mocha": true
  },
  "parserOptions": {
    "ecmaVersion": 2017
  },
  "extends": [
    "react-app",
    "plugin:jsx-a11y/recommended"
  ],
  "plugins": [
    "jsx-a11y"
  ]
}