# Boilerplate

## Start Developing

Before you can get started developing, you'll need to have the follow things installed:

* [NodeJS](https://nodejs.org)
* [yarn](https://yarnpkg.com/)

Once you have a local copy and have NodeJS and yarn installed, install the dependencies:

    cd [repo folder]
    yarn install
    yarn lerna bootstrap


## Directory structure

The directory and file structure is as follows:

* `docs` - documentation for the project
* `backend` - REST API
    * `src` - contains the source code
* `frontend` - UI Application
    * `src` - contains the source code



## Code Conventions
TODO

## Pull Request
TODO
### commit message format 

A detailed explanation can be found in [commit-message.md](./docs/commit-message.md).

## Releases

TODO